Docker test

Goal: learn docker with a project


For run app with docker  

1) run redis container: docker run -d --rm --name myredis redis
2) build python dockerfile. In python directory make: docker build -t python-prime-factors
3) run python container make: docker run --rm -p 5000:5000 --link myredis:redis python-prime-factors
4) connect your browser to 127.0.0.1:5000 and have fun !!!
