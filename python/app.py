from flask import Flask, render_template, request
from redis import Redis
import json

app = Flask(__name__, static_folder='static')
id = 0
my_list = []

def get_redis():
    redis = Redis(host="redis", db=0, socket_timeout=5)
    return redis

@app.route('/', methods=['GET', 'POST'])
def form():
    global id
    global my_list

    if request.method == 'POST':
        id += 1
        redis = get_redis()
        number = request.form.get('number')
        data = json.dumps({'id': id, 'number': number})
        redis.rpush('numbers', data)
        my_list = redis.lrange('numbers', 0, -1)
        print(my_list)

    return render_template('form.html')

@app.route('/hello')
def hello():
    return 'Hello world';

## pour test sur 127.0.0.1
