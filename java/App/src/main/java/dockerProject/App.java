package dockerProject;

import java.util.HashSet;
import java.util.Set;
import redis.clients.jedis.*;
import java.lang.Thread;

public class App 
{

	/*public static void main( String[] args)
	{
		System.out.println("Hello world");
	}*/

	static Set<String> keys = new HashSet<String>();

    public static void main( String[] args )
    {
        System.out.println("Hello World!");

		//var jedis = new Jedis("redis", 6379);
		Jedis jedis = new Jedis("localhost", 6379);
		System.out.println("Connection sucessfully");

		System.out.println("Server is running: " + jedis.ping());

		getNewData(jedis);

		jedis.close();
    }

    public static void primeFactors(int nb)
    {
		if(nb < 2)
		{
			System.out.println("Erreur, nombre trop petit");
		}
		else
		{
			for(int i = 2; i <= nb; i++)
			{
				while(nb % i == 0)
				{
					System.out.println(i);
					nb /= i;
				}
			}
		}
    }

    public static void sleep(long time)
    {
		try{
			Thread.sleep(time);
		}catch(InterruptedException e)
		{
			System.out.println("got interrupted");
		}
	    
    }

    public static void getNewData(Jedis jedis)
    {
	    Set<String> currentData = jedis.keys("id");

	    if(!currentData.containsAll(keys))
	    {
			currentData.removeAll(keys);

			for(String s : currentData)
			{
				primeFactors(Integer.parseInt(jedis.get(s)));
			}
	    }
    }
}
